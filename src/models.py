from peewee import CharField, Model, PostgresqlDatabase, DateField

db = PostgresqlDatabase('people.db')

class Person(Model):
    first_name = CharField()
    last_name = CharField()
    birthday = DateField()
    class Meta:
        database = db